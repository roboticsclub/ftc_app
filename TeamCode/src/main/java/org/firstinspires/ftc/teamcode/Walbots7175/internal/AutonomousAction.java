package org.firstinspires.ftc.teamcode.Walbots7175.internal;

/**
 * Created by nicolas on 1/28/18 in ftc_app.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/ftc_app
 * Contact: nico@walbots.com, team@walbots.com
 */


import org.firstinspires.ftc.teamcode.HardwareController;


/**
 * The abstract class AutonomousAction defines basic behaviour and functionality for
 * AutonomousFunctions. This provides a base for the AutonomousController to interact with various
 * types and different implementations (subclasses) of the AutonomousAction class giving the
 * opportunity to create big sets of individual actions that have a common standardized definition
 * and can be completed during Autonomous Mode.
 *
 * The life-cycle with the AutonomousController is:
 * - initialize()
 * - setup()
 * - run()          (can be called more than once)
 * - shutdown()
 */
public abstract class AutonomousAction
{
    /**
     * The hardware property gives AutonomousFunctions access to the hardware of the robot
     */
    public HardwareController hardware;

    /**
     * Instantiates an AutonomousAction with the robot's hardware and a descriptive name
     *
     * @param hardware The hardware controller of the robot
     * @param name The name of the instance of the AutonomousAction (will be displayed on the phone)
     */
    public AutonomousAction(HardwareController hardware, String name)
    {
        this.hardware = hardware;
        this.name = name;
    }

    /**
     * The ActionState enum defines the various states an AutonomousAction can be in
     */
    public enum ActionState
    {
        /**
         * The AutonomousAction is running, so the run() method will be called (continuously)
         */
        RUNNING,
        /**
         * The AutonomousAction is finished, so it is ready for shutdown()
         */
        FINISHED
    }


    /**
     * A descriptive name for the instance of the AutonomousAction implementation that will be
     * displayed on the phone for information purposes
     */
    public String name = "";

    /**
     * The amount of time that has to pass by in the life-cycle after the run() method returned
     * FINISHED and before the shutdown() method is called.
     */
    public long timeToWaitAfterFinished = 0;

    private long timeWhenFunctionStarted;

    /**
     * The abstract setup() method is overwritten by each implementation of the
     * AutonomousAction. It is called by the AutonomousController for setting the
     * AutonomousAction up and making it ready for the run() method
     */
    abstract public void setup();

    /**
     * The abstract run() method is overwritten by each implementation of the
     * AutonomousAction. It is continually called by the AutonomousController as long as it
     * returns RUNNING and until it returns FINISHED.
     *
     * In the implementation of this method the specific autonomous action is done. Once the action
     * is finished, the function returns FINISHED.
     *
     * @return RUNNING, FINISHED, (nil=error(=FINISHED))
     */
    abstract public ActionState run();

    /**
     * The abstract shutdown() method is overwritten by each implementation of the
     * AutonomousAction. It is called by the AutonomousController after the run() method is
     * finished and the timeToWaitAfterFinished has passed by.
     *
     * In the implementation of this method the autonomous action is cleaning up (stopping motors or
     * cleaning up and finishing up if needed)
     */
    abstract public void shutdown();

    /**
     * The initialize() method is called by the AutonomousController to start the life-cycle of the
     * AutonomousAction.
     *
     * The abstract setup() method is called and the current time is saved so that we know later for
     * how long a specific autonomous function is running.
     */
    final void initialize()
    {
        setup();
        timeWhenFunctionStarted = System.currentTimeMillis();
    }

    /**
     * The getRuntime() method calculates the time that passed since the setup() method was called
     * in the life-cycle of the AutonomousAction until the current point of time.
     *
     * @return The time (ms) for how long the AutonomousAction is running (since setup() finished)
     */
    final long getRuntime()
    {
        return System.currentTimeMillis() - timeWhenFunctionStarted;
    }
}

